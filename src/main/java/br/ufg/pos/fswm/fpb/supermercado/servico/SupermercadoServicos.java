package br.ufg.pos.fswm.fpb.supermercado.servico;

import br.ufg.pos.fswm.fpb.supermercado.modelo.Produto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 27/05/17.
 */
public class SupermercadoServicos {

    private static SupermercadoServicos instance;

    private List<Produto> produtos;

    private SupermercadoServicos() {
        produtos = new ArrayList<>();
    }

    public static SupermercadoServicos getInstanceOf() {
        if (instance == null) {
            instance = new SupermercadoServicos();
        }
        return instance;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void addProduto(Produto produto) {
        this.produtos.add(produto);
    }
}
