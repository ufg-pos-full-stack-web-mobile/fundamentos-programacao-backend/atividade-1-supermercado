package br.ufg.pos.fswm.fpb.supermercado.servlets;

import br.ufg.pos.fswm.fpb.supermercado.modelo.Produto;
import br.ufg.pos.fswm.fpb.supermercado.servico.SupermercadoServicos;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 27/05/17.
 */
@WebServlet(name = "carrinho", urlPatterns = {"/carrinho"})
public class CarrinhoServlet extends HttpServlet {

    public static final String UTF_8 = "UTF-8";
    public static final String TEXT_HTML = "text/html";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding(UTF_8);
        resp.setCharacterEncoding(UTF_8);
        resp.setContentType(TEXT_HTML);

        List<Produto> produtos = SupermercadoServicos.getInstanceOf().getProdutos();

        final StringBuilder sb = new StringBuilder();

        sb.append("<html>" +
                "<head>" +
                "<meta charset=\"utf-8\">" +
                "</head>" +
                "<body>" +
                "" +
                "<form action=\"carrinho\" method=\"post\">" +
                "<table>" +
                "<thead>" +
                "<tr>" +
                "<th>Check</th>" +
                "<th>Descrição</th>" +
                "<th>Preco</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>");

        for (Produto produto : produtos) {
            sb.append("<tr>" +
                    "<th><input type=\"checkbox\" name=\"marcado\"></th>" +
                    "<th>" + produto.getNome() + "</th>" +
                    "<th>R$ " + produto.getPreco() + "</th>" +
                    "</tr>");
        }

        sb.append("</tbody>" +
                "</table>" +
                "</form>" +
                "" +
                "</body>" +
                "" +
                "</html>");


        resp.getWriter().print(sb.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding(UTF_8);
        resp.setCharacterEncoding(UTF_8);
        resp.setContentType(TEXT_HTML);

        resp.getWriter().print("<html>" +
                "<head>" +
                "<meta charset=\"utf-8\">" +
                "</head>" +
                "<body>" +
                "<p>Fiz</p>" +
                "</body>" +
                "" +
                "</html>");
    }
}
